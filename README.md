# README #

For the research Work On
### Comment Analysis using Machine Learning and Natural Language processing, the following research papers are followed

[2010] Automatic Quality Assessment of Source Code Comments The JavadocMiner (Khamis et al)
[2010] Natural Language Parsing of Program Element Names for Concept Extraction (Abebe & Tonella)
[2011] Improving Identifier Informativeness Using Part of Speech Information (Binkley et al)
[2006] Towards Supporting On-Demand Virtual Remodularization Using Program Graphs (Shepherd et al)
[2006] HotComments - How to Make Program Comments More Useful (Tan et al)
etc.
You can find these papers in this repository...
